#!/bin/sh
set -e

echo "========================================================================="
echo "-------------------------------------------------------------------------"
echo "        Applying chimps for Docker ..."
echo "-------------------------------------------------------------------------"
# Change the uid/gid of the node user
if [ -n "${GID}" ]; then
    if [ -n "${UID}" ]; then
        echo " - Set the uid:gid of the node user to ${UID}:${GID}"
        groupmod -g ${GID} node && usermod -u ${UID} -g ${GID} node
    fi
fi

# Modify Shinobi configuration
echo "- Chimp Shinobi's technical configuration ..."
cd /opt/shinobi

echo "  - Set cpuUsageMarker ..."
node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" cpuUsageMarker="%Cpu(s)"
echo "-------------------------------------------------------------------------"
