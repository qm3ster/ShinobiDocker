#!/bin/sh
set -e

#   Update Shinobi's configuration by environment variables

echo "========================================================================="
echo "-------------------------------------------------------------------------"
echo "        Update Shinobi's configuration by environment variables"
echo "-------------------------------------------------------------------------"

# Set keys for CRON and PLUGINS ...
echo "Set keys for CRON and PLUGINS from environment variables ..."
if [ -n "${CRON_KEY}" ]; then
    echo "- Setting CRON key ..."
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" cron.key="${CRON_KEY}"
fi

if [ -n "${PLUGINKEY_MOTION}" ]; then
    echo "- Setting PLUGIN key ..."
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" pluginKeys.Motion="${PLUGINKEY_MOTION}"
fi

if [ -n "${PLUGINKEY_OPENCV}" ]; then
    echo "- Setting OPENCV key ..."
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" pluginKeys.OpenCV="${PLUGINKEY_OPENCV}"
fi

if [ -n "${PLUGINKEY_OPENALPR}" ]; then
    echo "- Setting OPENALPR key ..."
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" pluginKeys.OpenALPR="${PLUGINKEY_OPENALPR}"
fi

# Set configuration for motion plugin ...
echo "- Set configuration for motion plugin from environment variables ..."
node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/plugins/motion/conf.json" host="${MOTION_HOST}" port="${MOTION_PORT}" key="${PLUGINKEY_MOTION}"

# Set password hash type
echo "-------------------------------------------------------------------------"
if [ -n "${PASSWORD_HASH}" ]; then
    echo "- Set password hash type to ${PASSWORD_HASH} from environment variable PASSWORD_HASH !"
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" passwordType="${PASSWORD_HASH}"
fi

# Set the admin password
echo "-------------------------------------------------------------------------"
if [ -n "${ADMIN_USER}" ]; then
    if [ -n "${ADMIN_PASSWORD}" ]; then
        echo "Set the super admin credentials ..."
        # Hash the admins password
        export APP_PASSWORD_HASH=$( node -pe "require('./conf.json')['passwordType']" )
        if [ ! -n "${APP_PASSWORD_HASH}" ]; then
            export APP_PASSWORD_HASH="sha256"
        fi

        echo "  - Hash super admin password (${APP_PASSWORD_HASH})..."
        case "${APP_PASSWORD_HASH}" in
            md5)
                # MD5 hashing - unsecure!
                ADMIN_PASSWORD_HASH=$(echo -n "${ADMIN_PASSWORD}" | md5sum | sed -e 's/  -$//')
                ;;
            
            sha256)
                # SHA256 hashing
                ADMIN_PASSWORD_HASH=$(echo -n "${ADMIN_PASSWORD}" | sha256sum | sed -e 's/  -$//')
                ;;
            
            sha512)
                # SHA512 hashing with salting
                ADMIN_PASSWORD_HASH=$(echo -n "${ADMIN_PASSWORD}" | sha512sum | sed -e 's/  -$//')
                ;;

            *)
                echo "Unsupported password type ${APP_PASSWORD_HASH}. Set to md5, sha256 or sha512."
                exit 1
        esac

        # Set Shinobi's superuser's credentials
        echo "  - Set credentials ..."
        node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/super.json" 0.mail="${ADMIN_USER}" 0.pass="${ADMIN_PASSWORD_HASH}"
    fi
fi

echo "-------------------------------------------------------------------------"
echo "Set product type ... ${PRODUCT_TYPE}"
[ -n "${PRODUCT_TYPE}" ] && node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" productType="${PRODUCT_TYPE}"

echo "-------------------------------------------------------------------------"
echo "Set subscrpiton id ... ${SUBSCRIPTION_ID}"
[ -n "${SUBSCRIPTION_ID}" ] && node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" subscriptionId="${SUBSCRIPTION_ID}"
echo "========================================================================="
