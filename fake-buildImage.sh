#!/bin/bash

set -e

source ./fake-environment.env

export CONTAINER_TEST_IMAGE
export FAKE_CI

echo "==========================================================================="
echo "                          MICROSERVICE IMAGES"
echo "==========================================================================="

# Phusion image dprecated
/bin/bash ./gitlab-ci/buildImage.sh "$SHINOBI_BRANCHES" "$FAKE_BUILD_IMAGES"
